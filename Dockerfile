FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN set -xe \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
        supervisor openbox feh tigervnc-standalone-server tigervnc-xorg-extension x11-utils \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY wallpaper.jpg /var/local/wallpaper.jpg

EXPOSE 5900

CMD ["/usr/bin/supervisord"]

